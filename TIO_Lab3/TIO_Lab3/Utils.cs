﻿using System.Linq;

namespace TIO_Lab3
{
    public static class Utils
    {
        public static bool AreIntArraysEqual(int[] array1, int[] array2)
        {
            if (array1 == null && array2 == null)
                return true;
            if (array1 == null || array2 == null)
                return false;
            if (array1.Length != array2.Length)
                return false;

            return !array1.Where((t, i) => t != array2[i]).Any();
        }
    }
}