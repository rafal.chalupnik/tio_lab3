﻿using System;
using TIO_Lab3.Data;

namespace TIO_Lab3.Algorithms
{
    public interface IAlgorithm
    {
        Genotype Run(Func<int, bool> stopCondition);
    }
}