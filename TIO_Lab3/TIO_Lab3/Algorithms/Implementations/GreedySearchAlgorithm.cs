﻿using System;
using System.Collections.Generic;
using System.Linq;
using TIO_Lab3.Data;
using TIO_Lab3.Data.Factory;
using TIO_Lab3.Logging;

namespace TIO_Lab3.Algorithms.Implementations
{
    public class GreedySearchAlgorithm : IAlgorithm
    {
        private readonly IGenotypeFactory genotypeFactory;
        private readonly ILogger logger;

        public GreedySearchAlgorithm(IGenotypeFactory genotypeFactory, ILogger logger)
        {
            this.genotypeFactory = genotypeFactory;
            this.logger = logger;
        }

        public Genotype Run(Func<int, bool> stopCondition = null)
        {
            var bestSolution = genotypeFactory.CreateRandom();

            var iteration = 0;
            var atLeastOneSwapped = true;

            while (atLeastOneSwapped)
            {
                iteration++;
                atLeastOneSwapped = false;

                var pairs = GetSwapPairs(bestSolution.Size);

                while (pairs.Count != 0)
                {
                    var pair = pairs.Dequeue();

                    var solution = CloneAndSwap(bestSolution, pair.Item1, pair.Item2);

                    if (solution.Value < bestSolution.Value)
                    {
                        bestSolution = solution;
                        atLeastOneSwapped = true;
                    }
                }

                logger.LogIteration(iteration, bestSolution.Value);
            }

            return bestSolution;
        }

        private static Queue<Tuple<int, int>> GetSwapPairs(int size)
        {
            var queue = new Queue<Tuple<int, int>>();

            for (var i = 0; i < size; i++)
            {
                var greaterThanI = Enumerable.Range(i + 1, size - i - 1);

                foreach (var second in greaterThanI)
                {
                    queue.Enqueue(Tuple.Create(i, second));
                }
            }

            return queue;
        }

        private static Genotype CloneAndSwap(Genotype genotype, int index1, int index2)
        {
            var clone = (Genotype) genotype.Clone();
            clone.Swap(index1, index2);

            return clone;
        }
    }
}