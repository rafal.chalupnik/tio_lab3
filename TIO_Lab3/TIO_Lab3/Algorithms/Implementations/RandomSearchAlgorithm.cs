﻿using System;
using TIO_Lab3.Data;
using TIO_Lab3.Data.Factory;
using TIO_Lab3.Logging;

namespace TIO_Lab3.Algorithms.Implementations
{
    public class RandomSearchAlgorithm : IAlgorithm
    {
        private readonly IGenotypeFactory genotypeFactory;
        private readonly ILogger logger;

        public RandomSearchAlgorithm(IGenotypeFactory genotypeFactory, ILogger logger)
        {
            this.genotypeFactory = genotypeFactory;
            this.logger = logger;
        }

        public Genotype Run(Func<int, bool> stopCondition)
        {
            var iteration = 0;

            var bestValue = int.MaxValue;
            Genotype bestGenotype = null;

            while (!stopCondition(iteration))
            {
                iteration++;

                var genotype = genotypeFactory.CreateRandom();

                if (genotype.Value < bestValue)
                {
                    bestGenotype = genotype;
                    bestValue = genotype.Value;
                }

                logger.LogIteration(iteration, bestValue);
            }

            return bestGenotype;
        }
    }
}