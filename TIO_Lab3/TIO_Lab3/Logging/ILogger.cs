﻿using System;

namespace TIO_Lab3.Logging
{
    public interface ILogger : IDisposable
    {
        void LogIteration(int iteration, int bestScore);
    }
}