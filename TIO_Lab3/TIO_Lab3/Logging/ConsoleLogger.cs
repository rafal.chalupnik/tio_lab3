﻿using System;

namespace TIO_Lab3.Logging
{
    public class ConsoleLogger : ILogger
    {
        public void LogIteration(int iteration, int bestScore)
        {
            Console.WriteLine($"Iteration {iteration}: {bestScore}");
        }

        public void Dispose()
        {
            Console.WriteLine("Log closed.");
        }
    }
}