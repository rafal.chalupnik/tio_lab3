﻿namespace TIO_Lab3.Logging
{
    public class ComboLogger : ILogger
    {
        private readonly CsvLogger csvLogger;
        private readonly ConsoleLogger consoleLogger;

        public ComboLogger(CsvLogger csvLogger, ConsoleLogger consoleLogger)
        {
            this.csvLogger = csvLogger;
            this.consoleLogger = consoleLogger;
        }

        public void Dispose()
        {
            csvLogger.Dispose();
            consoleLogger.Dispose();
        }

        public void LogIteration(int iteration, int bestScore)
        {
            csvLogger.LogIteration(iteration, bestScore);
            consoleLogger.LogIteration(iteration, bestScore);
        }
    }
}