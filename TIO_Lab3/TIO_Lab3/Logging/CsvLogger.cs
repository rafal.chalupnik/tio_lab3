﻿using System.Collections.Generic;
using System.IO;

namespace TIO_Lab3.Logging
{
    public class CsvLogger : ILogger
    {
        private readonly List<string> content;
        private readonly string filePath;

        public CsvLogger(string filePath)
        {
            this.filePath = filePath;

            content = new List<string>
            {
                "Iteration,Score"
            };
        }

        public void LogIteration(int iteration, int bestScore)
        {
            content.Add($"{iteration},{bestScore}");
        }

        public void Dispose()
        {
            File.WriteAllLines(filePath, content);
        }
    }
}