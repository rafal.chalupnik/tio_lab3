﻿using System;
using System.Collections.Generic;
using System.Linq;
using TIO_Lab3.Algorithms;
using TIO_Lab3.Algorithms.Implementations;
using TIO_Lab3.Data;
using TIO_Lab3.Data.Evaluation;
using TIO_Lab3.Data.Factory;
using TIO_Lab3.Data.Loader;
using TIO_Lab3.Logging;

namespace TIO_Lab3
{
    public class Program
    {
        private const int GenotypeSize = 20;
        private const string LogFilePath = "log.csv";

        private static readonly string DataFilePath = $@"Files\had{GenotypeSize}.dat";
        private static readonly TimeSpan Duration = TimeSpan.FromSeconds(30);

        public static void Main(string[] args)
        {
            var dataLoader = CreateDataLoader();
            var random = new Random();
            var evaluator = CreateEvaluator(dataLoader);
            var genotypeFactory = CreateGenotypeFactory(random, evaluator);

            using (var logger = CreateComboLogger())
            {
                var algorithm = CreateHybridSearchAlgorithm(genotypeFactory, logger);

                var stopCondition = CreateStopCondition();
                var result = algorithm.Run(stopCondition);

                Console.WriteLine($"Result: {result}");
            }

            Console.WriteLine("### Done! ###");
            Console.ReadLine();
        }

        private static IDataLoader CreateDataLoader()
        {
            return new DataLoader(DataFilePath);
        }

        private static IEvaluator CreateEvaluator(IDataLoader dataLoader)
        {
            return new GenotypeEvaluator(dataLoader);
        }

        private static IGenotypeFactory CreateGenotypeFactory(Random random, IEvaluator evaluator)
        {
            return new GenotypeFactory(GenotypeSize, random, evaluator);
        }

        private static ILogger CreateComboLogger()
        {
            return new ComboLogger(
                new CsvLogger(LogFilePath),
                new ConsoleLogger());
        }

        private static IAlgorithm CreateGreedySearchAlgorithm(IGenotypeFactory genotypeFactory, ILogger logger)
        {
            return new GreedySearchAlgorithm(genotypeFactory, logger);
        }

        private static IAlgorithm CreateHybridSearchAlgorithm(IGenotypeFactory genotypeFactory, ILogger logger)
        {
            return new HybridSearchAlgorithm(genotypeFactory, logger);
        }

        private static IAlgorithm CreateRandomSearchAlgorithm(IGenotypeFactory genotypeFactory, ILogger logger)
        {
            return new RandomSearchAlgorithm(genotypeFactory, logger);
        }

        private static Func<int, bool> CreateStopCondition()
        {
            var endTime = DateTime.Now + Duration;

            return iteration => DateTime.Now > endTime;
        }
    }
}