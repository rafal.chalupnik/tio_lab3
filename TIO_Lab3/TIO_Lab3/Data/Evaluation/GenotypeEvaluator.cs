﻿using System;
using System.Collections.Generic;
using TIO_Lab3.Data.Loader;

namespace TIO_Lab3.Data.Evaluation
{
    public class GenotypeEvaluator : IEvaluator
    {
        private readonly List<Tuple<Genotype, int>> cache;
        private readonly IDataLoader dataLoader;
        
        public GenotypeEvaluator(IDataLoader dataLoader)
        {
            cache = new List<Tuple<Genotype, int>>();
            this.dataLoader = dataLoader;
            GenotypeSize = dataLoader.FlowMatrix.GetLength(0);
        }

        public int Counter { get; private set; }

        public int GenotypeSize { get; }

        public int Evaluate(Genotype genotype)
        {
            foreach (var tuple in cache)
            {
                if (tuple.Item1.Equals(genotype))
                    return tuple.Item2;
            }

            var score = CalculateScore(genotype);
            Counter++;
            cache.Add(Tuple.Create(genotype, score));
            return score;
        }

        private int CalculateScore(Genotype genotype)
        {
            var list = genotype.AsList();
            var cost = 0;

            for (var r = 0; r < GenotypeSize; r++)
            {
                for (var c = 0; c < GenotypeSize; c++)
                {
                    cost += dataLoader.FlowMatrix[r, c] * dataLoader.LocationMatrix[list.IndexOf(r), list.IndexOf(c)];
                }
            }

            return cost;
        }
    }
}