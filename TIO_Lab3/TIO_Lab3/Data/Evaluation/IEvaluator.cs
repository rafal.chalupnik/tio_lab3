﻿namespace TIO_Lab3.Data.Evaluation
{
    public interface IEvaluator
    {
        int GenotypeSize { get; }

        int Evaluate(Genotype genotype);
    }
}