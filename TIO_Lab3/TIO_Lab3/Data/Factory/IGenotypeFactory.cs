﻿namespace TIO_Lab3.Data.Factory
{
    public interface IGenotypeFactory
    {
        Genotype CreateRandom();
    }
}