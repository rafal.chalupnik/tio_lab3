﻿using System;
using System.Linq;
using TIO_Lab3.Data.Evaluation;

namespace TIO_Lab3.Data.Factory
{
    public class GenotypeFactory : IGenotypeFactory
    {
        private readonly int genotypeSize;
        private readonly Random random;
        private readonly IEvaluator evaluator;

        public GenotypeFactory(int genotypeSize, Random random, IEvaluator evaluator)
        {
            this.genotypeSize = genotypeSize;
            this.random = random;
            this.evaluator = evaluator;
        }

        public Genotype CreateRandom()
        {
            var available = Enumerable.Range(0, genotypeSize).ToList();

            var solution = CreateGenotype();
            for (var i = 0; i < genotypeSize; i++)
            {
                solution[i] = available[random.Next(available.Count)];
                available.Remove(solution[i]);
            }

            return solution;
        }

        public Genotype CreateGenotype()
        {
            return new Genotype(evaluator, genotypeSize);
        }
    }
}