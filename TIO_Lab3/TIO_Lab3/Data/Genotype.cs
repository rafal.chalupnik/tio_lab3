﻿using System;
using System.Collections.Generic;
using System.Linq;
using TIO_Lab3.Data.Evaluation;

namespace TIO_Lab3.Data
{
    public class Genotype : ICloneable
    {
        private readonly IEvaluator evaluator;

        private readonly int[] genotype;

        public Genotype(IEvaluator evaluator, int genotypeSize)
        {
            this.evaluator = evaluator;

            genotype = new int[genotypeSize];
            Size = genotypeSize;
        }

        public int this[int index]
        {
            get => genotype[index];
            set => genotype[index] = value;
        }
        
        public int Size { get; }

        public int Value => evaluator.Evaluate(this);

        public List<int> AsList()
        {
            return genotype.ToList();
        }
        
        public object Clone()
        {
            var newGenotype = new Genotype(evaluator, Size);

            for (var i = 0; i < Size; i++)
            {
                newGenotype[i] = genotype[i];
            }

            return newGenotype;
        }

        public override bool Equals(object obj)
        {
            if (obj is Genotype otherGenotype)
            {
                for (var i = 0; i < Size; i++)
                {
                    if (genotype[i] != otherGenotype[i])
                        return false;
                }

                return true;
            }
            else
                return false;
        }
        
        public void Swap(int index1, int index2)
        {
            var temp = genotype[index1];
            genotype[index1] = genotype[index2];
            genotype[index2] = temp;
        }

        public override string ToString()
        {
            return $"{string.Join(", ", genotype)};\t{Value}";
        }
    }
}