﻿using System;
using System.IO;
using System.Linq;

namespace TIO_Lab3.Data.Loader
{
    public class DataLoader : IDataLoader
    {
        private const int FlowMatrixStartLine = 2;
        private const int SizeLine = 0;
        
        public int[,] FlowMatrix { get; }
        public int[,] LocationMatrix { get; }

        public DataLoader(string filePath)
        {
            if (string.IsNullOrEmpty(filePath))
                throw new ArgumentException("Ścieżka do pliku nie może być pusta!");

            var lines = File.ReadAllLines(filePath);

            var size = Convert.ToInt32(lines[SizeLine].Split(' ').First(_s => _s.Length != 0));

            FlowMatrix = new int[size, size];
            LocationMatrix = new int[size, size];

            var flowMatrixEndLine = FlowMatrixStartLine + size - 1;

            for (var i = FlowMatrixStartLine; i <= flowMatrixEndLine; i++)
            {
                var lineValues = lines[i].Split(' ').Where(_s => _s.Length != 0).ToList();
                if (lineValues.Count != size)
                    throw new FileLoadException("Plik jest uszkodzony!");
                for (var j = 0; j < size; j++)
                    FlowMatrix[i - FlowMatrixStartLine, j] = Convert.ToInt32(lineValues[j]);
            }

            var locationMatrixStartLine = flowMatrixEndLine + 2;
            var locationMatrixEndLine = locationMatrixStartLine + size - 1;

            for (var i = locationMatrixStartLine; i <= locationMatrixEndLine; i++)
            {
                var lineValues = lines[i].Split(' ').Where(_s => _s.Length != 0).ToList();
                if (lineValues.Count != size)
                    throw new FileLoadException("Plik jest uszkodzony!");
                for (var j = 0; j < size; j++)
                    LocationMatrix[i - locationMatrixStartLine, j] = Convert.ToInt32(lineValues[j]);
            }
        }
    }
}