﻿namespace TIO_Lab3.Data.Loader
{
    public interface IDataLoader
    {
        int[,] FlowMatrix { get; }
        
        int[,] LocationMatrix { get; }
    }
}